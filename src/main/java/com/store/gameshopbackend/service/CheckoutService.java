package com.store.gameshopbackend.service;

import com.store.gameshopbackend.dto.Purchase;
import com.store.gameshopbackend.dto.PurchaseResponse;
import org.springframework.stereotype.Service;


public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);
}
