package com.store.gameshopbackend.dto;


import lombok.Data;

@Data
public class PurchaseResponse {

    private final String orderTrackingNumber;
}
