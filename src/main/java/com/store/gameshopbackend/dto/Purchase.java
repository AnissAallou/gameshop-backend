package com.store.gameshopbackend.dto;

import com.store.gameshopbackend.entity.Address;
import com.store.gameshopbackend.entity.Customer;
import com.store.gameshopbackend.entity.Order;
import com.store.gameshopbackend.entity.OrderItem;
import lombok.Data;

import java.util.Set;

@Data
public class Purchase {

    private Customer customer;
    private Address shippingAddress;
    private Address billingAddress;
    private Order order;
    private Set<OrderItem> orderItems;

}
