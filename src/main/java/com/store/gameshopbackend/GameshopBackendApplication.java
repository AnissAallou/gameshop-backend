package com.store.gameshopbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameshopBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameshopBackendApplication.class, args);
	}

}
